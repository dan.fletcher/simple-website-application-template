<?php
require_once("header.php");               // imports the header which require_onces providing login security
require_once("menu_main.html");            // all menus will require_once main menu which require_onces authentication
check_group("User Admin", "deny.php");   // Security Group for this function
?>



<p>Here you can Add or modify users on the system</p>


<p>Existing Users</p>


<?php

print '<form method="post">';
print '<input type="submit" name="action" value="New User">';
print '</form>';


// First we get any url values if set
if (isset($_GET['id'])) $id = $_GET['id'];
else $id = "";

// If this was a post save data and check for error
if (!empty($_POST)){

  if ($_POST['action'] == 'Update'){
	// we have to remove the action value before we send it
	unset($_POST['action']);

  	if (dbupdate('users', $_POST)) Print '<font color="green">Update Sucessful</font>';
  	else Print '<font color="red">Update Failed</font>';

	$return = dbquery("SELECT * FROM users");
	print printtablewid($return);
  }
  elseif ($_POST['action'] == 'Delete'){
        if (dbdelete('users', $_POST)) Print '<font color="green">Delete Sucessful</font>';
        else Print '<font color="red">Update Failed</font>';

        $return = dbquery("SELECT * FROM users");
        print printtablewid($return);
  }
  elseif ($_POST['action'] == 'New User'){
	Print "<form method=\"post\">";
	Print "<p>Firstname : <input type=\"text\" name=\"firstname\" value=\"\">";
	Print "<p>Lastname  : <input type=\"text\" name=\"lastname\" value=\"\">";
	Print "<p>Login     : <input type=\"text\" name=\"email\" value=\"\">";
	Print "<p>Password  : <input type=\"password\" name=\"password\" value=\"\">";
	Print "<p><input type=\"submit\" name=\"action\" value=\"Add User\"></p>";
	Print "</form>";
  }
  elseif ($_POST['action'] == 'Add User'){
        unset($_POST['action']);
        $_POST['password'] = md5($_POST['password']);
        if (dbinsert('users', $_POST)) Print '<font color="green">Add Sucessful</font>';
        else Print '<font color="red">Update Failed</font>';

        $return = dbquery("SELECT * FROM users");
        print printtablewid($return);
  }
}
else {

  // Check if a specifc id was requested
  if ($id==""){
    // If not id query all the users
    $return = dbquery("SELECT * FROM users");

    // Print a table with id's
    print printtablewid($return);
  }
  else
  {
    // If a specific id was requested select records from that id only
    $return=dbquery("SELECT * FROM users where id=$id");

    // Print a edit table this time
    print printedit($return);
  }
}
?>


<?php require_once("footer.html"); ?>
