<?php


function dbconn(){
  // Include the auth variables
  include('config.php');

  // Pull in global variable
  global $dbconn;

  // Connecting, selecting database
  $dbconn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die ('Error connecting to MySQL server');
}


// This function will return any sql query as an array
function dbquery($sql)
{
  // Pull in the global variable
  global $dbconn;

  // Performing SQL query
  //print($sql);
  $query = mysqli_query($dbconn, $sql) or die('Query failed: ' . mysql_last_error());

  // Now return the array
  $return = array();

    while($row = mysqli_fetch_assoc($query))
    {
    $a = array();
      foreach($row as $key => $value)
      {
         $a[$key] = $value;
      }
    array_push($return, $a);
    }

    return $return;
}

// This function will update any table entry matching ID
function dbupdate($table, $array)
{
  // Pull in the global variables
  global $dbconn;

  $sql = 'UPDATE '.$table.' SET ';
  foreach ($array as $key => $value)
  {
    if ($key != 'id' )
      {
        $sql .= $key.'="'.$value.'", ';
      }
  }

  $sql = substr($sql, 0, -2).' WHERE id='.$array['id'];

  $return = mysqli_query($dbconn, $sql) or die(mysql_error());

  return $return;

}

// This function will delete any table entry matching ID
function dbdelete($table, $array)
{
  // Pull in global variables
  global $dbconn;

  $sql = 'DELETE FROM '.$table.' WHERE id='.$array['id'];

  $return = mysqli_query($dbconn, $sql) or die(mysql_error());

  return $return;
}


// This function will delete any table entry matching ID
function dbinsert($table, $array)
{
  // Pull in global variable
  global $dbconn;

  $sql = 'INSERT INTO '.$table.' SET ';
  foreach ($array as $key => $value)
  {
      $sql .= $key.'="'.$value.'", ';
  }

  $sql = substr($sql, 0, -2);

  $return = mysqli_query($dbconn, $sql) or die(mysql_error());

  return $return;

}

?>
