<?php

//These thing need running for every session_start
error_reporting(E_ALL);
ini_set('display_errors', 1);

//Start session variables
session_start();

// Include all functions
include_once('includes.php');

//Establish database connection
dbconn();

?>
