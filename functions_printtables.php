<?php

// This will turn any array into a table (we don't account for nested arrray
function printtable($array)
{
  $return = '<table border="1">';
  $return .='<thead>';
  $return .='<tr>';
  $return .='<th>'.implode('</th><th>', array_keys(current($array))).'</th>';
  $return .='</tr>';
  $return .='</thead>';
  $return .='<tbody>';

  foreach ($array as $row)
  {
    array_map('htmlentities', $row);
    $return .='<tr>';
    $return .='<td>'.implode('</td><td>', $row).'</td>';
    $return .='</tr>';
  }

  $return .= '</tbody>';
  $return .= '</table>';

  return $return;
}

function printtablewid($array)
{
  $return = '<table border="1">';
  $return .='<thead>';
  $return .='<tr>';
  $return .='<th>'.implode('</th><th>', array_keys(current($array))).'</th>';
  $return .='</tr>';
  $return .='</thead>';
  $return .='<tbody>';

  foreach ($array as $row)
  {
    array_map('htmlentities', $row);
//    $return .='<a href="?id='.$row['id'].'">';
    $return .='<tr align="center" onclick="document.location=\'?id='.$row['id'].'\'" style="cursor:hand">';
    $return .='<td>'.implode('</td><td>', $row).'</td>';
    $return .='</tr>';
  }

  $return .= '</tbody>';
  $return .= '</table>';

  return $return;
}

function printedit($array)
{
  $return = "";
  foreach ($array as $row)
  {
    $return .= '<table><form action="?action=save&id='.$row['id'].'" method="post">';

    foreach ($row as $key => $value)
    {
      if ($key == "id"){
        $return .= '<tr>';
        $return .= '<td></td>';
        $return .= '<td><input type="text" hidden name="'.$key.'" value="'.$value.'"></td>';
        $return .= '</tr>';
      } else
      {
        $return .= '<tr>';
        $return .= '<td>'.$key.'</td>';
        //$return .= '<td><input type="text" name="'.$key.'" value="'.$value.'"></td>';
	$return .= '<td><textarea rows=2 cols=50 name="'.$key.'">'.$value.'</textarea></td>';
        $return .= '</tr>';
      }
    }
    $return .= '<tr><td></td><td><input type="submit" name="action" value="Update">';
    $return .= '<input type="submit" name="action" value="Delete"></td></tr>';
    $return .= '</form></table>';
  }

  return $return;
}

?>
