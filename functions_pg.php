<?php

function dbconn(){
  // Include the auth variables
  include('config.php');

  // Pull in global variable
  global $dbconn;

  // Connecting, selecting database
  $dbconn = pg_connect("host=$dbhost dbname=$dbname user=$dbuser password=$dbpass")
      or die('Could not connect: ' . pg_last_error());
}


// This function will return any sql query as an array
function dbquery($sql)
{
  // Pull in the global variable
  global $dbconn;

  // Performing SQL query
  //print($sql);
  $query = pg_query($sql) or die('Query failed: ' . pg_last_error());

  // Now return the array
  $return = array();

    while($row = pg_fetch_assoc($query))
    {
    $a = array();
      foreach($row as $key => $value)
      {
         $a[$key] = $value;
      }
    array_push($return, $a);
    }
    print_r($return);
    return $return;
}


// This function will update any table entry matching ID
function dbupdate($table, $array)
{
  // Pull in the global variables
  global $dbconn;

  // Post array as an update
  $return = pg_update($dbconn, $table, $array, array('id' => $array['id']));

  return $return;
}


// This function will delete any table entry matching ID
function dbdelete($table, $array)
{
  // Pull in global variables
  global $dbconn;

  // Post array as an update
  $return = pg_delete($dbconn, $table, array('id' => $array['id']));

  return $return;
}


// This function will delete any table entry matching ID
function dbinsert($table, $array)
{
  // Pull in global variable
  global $dbconn;

  // Post array as an update
$return = pg_insert($dbconn, $table, array('id' => $array['id']));

  return $return;
}


 ?>
